// The objective of this file is to install Adobe Camera Raw v12
#include "Photoshop.h"
#include "Cameraraw.h"

#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

int install_cameraraw()
{
    system("notify-send 'Photoshop CC' 'Starting Installer of Camera Raw' --icon=$PWD/images/photoshop-cc.png");
    char *location = malloc(sizeof(char) * 150);
    check_memory_allocation(location);
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    scanf("%s", location);
    printf("Installing Adobe Camera Raw v12...");
    // Download Camera Raw .exe
    int max_len = sizeof(char) * 350;
    char *command = malloc(max_len);
    check_memory_allocation(command);
    snprintf(command, max_len, "cd %s/PhotoshopCC; wget https://download.adobe.com/pub/adobe/photoshop/cameraraw/win/12.x/CameraRaw_12_2_1.exe", location);
    system(command);
    free(command);
    // Install
    max_len = sizeof(char) * 350;
    command = malloc(max_len);
    check_memory_allocation(command);
    snprintf(command, max_len, "export WINEPREFIX=%1$s/PhotoshopCC; cd %1$s/PhotoshopCC; wine CameraRaw_12_2_1.exe", location);
    system(command);
    // Free memory of location and command
    free(location);
    free(command);
    printf("Adobe Camera Raw v12 installed successfully!");
    return_program();
};